extern crate gtk;

pub mod gui {
	use gtk::prelude::*;
	
	pub fn start (app: &gtk::Application) {	
		let window: gtk::ApplicationWindow 
				= gtk::ApplicationWindowBuilder::new()
						.application(app)
						.build();
		window.set_title("Project Helper");
		window.set_default_size(600, 450);
		
		let notebook: gtk::Notebook
				= gtk::NotebookBuilder::new()
						.margin(12)
						.build();
						
		let lbl_project 
				= gtk::Label::new_with_mnemonic(Some("Project"));
		let lbl_installed 
				= gtk::Label::new_with_mnemonic(
						Some("Installed Packages"));
		let lbl_available 
				= gtk::Label::new_with_mnemonic(
						Some("Available Packages"));
		
		notebook.append_page(&page_project(), Some(&lbl_project));
		notebook.append_page(&page_installed(), Some(&lbl_installed));
		notebook.append_page(&page_available(), Some(&lbl_available));
		
		window.add(&notebook);
		
		window.show_all();
	}
	
	fn page_project() -> gtk::Box {
		let box_0: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.build();
		box_0.set_orientation(gtk::Orientation::Horizontal);
		
		let box_1: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.hexpand(true)
						.build();
		box_1.set_orientation(gtk::Orientation::Vertical);
		
		let box_2: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.build();
		box_2.set_orientation(gtk::Orientation::Horizontal);
		
		let box_3: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.build();
		box_3.set_orientation(gtk::Orientation::Vertical);
		
		let cmb: gtk::ComboBoxText
				= gtk::ComboBoxTextBuilder::new()
						.hexpand(true)
						.margin(12)	
						.build();
						
		let file_chooser: gtk::Button
				= gtk::ButtonBuilder::new()
						.label("Browse Files")
						.margin(12)
						.build();
						
		let scroll: gtk::ScrolledWindow
				= gtk::ScrolledWindowBuilder::new()
						.vexpand(true)
						.margin(12)
						.build();
						
		let btn_init: gtk::Button
				= gtk::ButtonBuilder::new()
						.label("Initialise Project")
						.margin(12)
						.build();
						
		let btn_lint: gtk::Button
				= gtk::ButtonBuilder::new()
						.label("Lint")
						.margin(12)
						.build();
						
		let txtview: gtk::TextView
				= gtk::TextViewBuilder::new()
						.vexpand(true)
						.build();
						
		box_0.add(&box_1);
		box_0.add(&box_3);
		
		box_1.add(&box_2);
		box_1.add(&scroll);
		
		box_2.add(&cmb);
		box_2.add(&file_chooser);
		
		box_3.add(&btn_init);
		box_3.add(&btn_lint);
		
		scroll.add(&txtview);
		
		box_0
	}
	
	fn page_installed() -> gtk::Box {
		let box_0: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.build();
		box_0.set_orientation(gtk::Orientation::Vertical);

		let box_1: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.hexpand(true)
						.build();
		box_1.set_orientation(gtk::Orientation::Horizontal);
		
		let scroll: gtk::ScrolledWindow
				= gtk::ScrolledWindowBuilder::new()
						.margin(12)
						.vexpand(true)
						.build();
						
		let listbox: gtk::ListBox
				= gtk::ListBoxBuilder::new()
						.margin(12)
						.vexpand(true)
						.build();
						
		let btn_update: gtk::Button
				= gtk::ButtonBuilder::new()
						.label("Update")
						.margin(12)
						.hexpand(true)
						.build();
						
		let btn_remove: gtk::Button
				= gtk::ButtonBuilder::new()
						.label("Remove")
						.margin(12)
						.hexpand(true)
						.build();
						
		box_0.add(&box_1);
		box_0.add(&scroll);
		
		box_1.add(&btn_update);
		box_1.add(&btn_remove);
		
		scroll.add(&listbox);
		
		box_0
	}
	
	fn page_available() -> gtk::Box {
		let box_0: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.build();
		box_0.set_orientation(gtk::Orientation::Vertical);

		let box_1: gtk::Box
				= gtk::BoxBuilder::new()
						.margin(12)
						.hexpand(true)
						.build();
		box_1.set_orientation(gtk::Orientation::Horizontal);
		
		let scroll: gtk::ScrolledWindow
				= gtk::ScrolledWindowBuilder::new()
						.margin(12)
						.vexpand(true)
						.build();
						
		let listbox: gtk::ListBox
				= gtk::ListBoxBuilder::new()
						.margin(12)
						.vexpand(true)
						.build();
						
		let btn_install: gtk::Button
				= gtk::ButtonBuilder::new()
						.label("Install")
						.margin(12)
						.hexpand(true)
						.build();
						
		box_0.add(&box_1);
		box_0.add(&scroll);
		
		box_1.add(&btn_install);
		
		scroll.add(&listbox);
		
		box_0
	}
}
