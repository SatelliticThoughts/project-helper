extern crate gtk;
extern crate gio;

use gio::prelude::*;

use gtk::{Application};


use crate::guimain::gui::start;
mod guimain;

fn main() {
    let application = Application::new(
			Some("com.gitlab.SatelliticThoughts.project-helper"), 
			Default::default()
		).expect("failed to initialize GTK application");

    application.connect_activate(|app| { start(&app); });
    application.run(&[]);
}
